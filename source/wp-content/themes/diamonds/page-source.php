<?php
/*
Template Name: Source
*/

get_header(); ?>

<?php
while (have_posts()) : the_post();
    ?>
    <body class="source">
    <?php include '_nav.php'; ?>
    <main>
        <?php include '_content-loading.php'; ?>
        <section>
            <div class="source-bg" style="background-image: url(<?php echo get_field('banner_image'); ?>); !important;"></div>
            <div class="source__s01">
                <div class="container div-center div-center--top animated fadeIn">
                    <div data-anchor-target=".source__s01__desc" data-top-bottom="opacity:0" data-center="opacity:1" class="row source__s01__desc">
                        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 text-xs-center">
                            <h1 class="sr-only"><?php the_title(); ?></h1>
                            <div class="text--lg white text--shadow"><?php echo the_content(); ?></div>
                        </div>
                        <div class="arrow-down">
                            <button id="go-to-next" class="btn--arrow-down btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="Go down"></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-white">
            <div id="next__anchor" class="container bg-white">
                <div class="row pt-3">
                    <div class="col-xs-12 col-sm-6 text-xs-center">
                        <div class="source__s01__text animated-desktop fadeInLeftSoon">
                            <div class="pb-1"><?php echo get_field('content_1_1'); ?></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <img src="<?php echo get_field('image_1_1'); ?>" class="source__s01__img01 animated-desktop fadeInRightSoon">
                    </div>
                </div>
                <div class="row pb-3">
                    <div class="col-xs-12 col-sm-7">
                        <img src="<?php echo get_field('image_1_2'); ?>" alt="TheSource_Image02" class="source__s01__img02 animated-desktop fadeInLeftSoon">
                    </div>
                    <div class="col-xs-12 col-sm-5 text-xs-center">
                        <div class="source__s01__text02 pr-1 pt-1 animated-desktop fadeInRightSoon">
                            <div class="mb-0"><?php echo get_field('content_1_2'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="source__s01__testimonial text-xs-center">
                <div class="container py-3 mt-2">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 offset-sm-2">
                            <h2 class="title"><?php echo get_field('title_2'); ?></h2>
                            <div><?php echo get_field('content_2_1'); ?></div>
                        </div>
                    </div>
                    <div class="row testimony">
                        <div class="col-xs-12 col-sm-6 animated-desktop fadeInLeftSoon">
                            <img src="<?php echo get_field('image_2'); ?>" alt="<?php echo get_field('title_2'); ?>">
                        </div>
                        <div class="col-xs-12 col-sm-6 animated-desktop fadeInRightSoon">
                            <div><?php echo get_field('content_2_2'); ?></div>
                            <!-- <p class="text--italic testimonial-quote">“I started my business through Argyle and have developed my reputation as a tour operator and cross-cultural training provider, developing business for the long term. I am now even expanding my business beyond Argyle into more cross-cultural training programs and some 4WD tours.”</p>
                            <p>Ted Hall, a Miriuwung business owner</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php endwhile; ?>

<?php get_footer(); ?>