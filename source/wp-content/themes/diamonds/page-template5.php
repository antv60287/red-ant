<?php
/*
Template Name: Template 5
*/

get_header(); ?>

<?php
while (have_posts()) : the_post();
  ?>
  <body class="story">
  <?php include '_nav.php'; ?>
  <main>
  	<?php include '_content-loading.php'; ?>
    <?php include_once '_content-template5.php'; ?>
  </main>
<?php endwhile; ?>

<?php get_footer(); ?>