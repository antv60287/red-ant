<div class="loading">
      <svg id="loaderSvg" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 115.57 101.72">
      <defs>
      <style>.cls-1{fill:#e1854f;}</style>
      </defs>
      <title>Loader</title>
      <polygon id="shape-8-1" points="40.97 37.6 56.9 6.63 24.31 1.72 21.84 24.09 40.97 37.6" class="cls-1"></polygon>
      <polygon id="shape-1-1" points="91.35 1.73 59.58 6.63 74.69 37.6 93.82 24.09 91.35 1.73" class="cls-1"></polygon>
      <polygon id="shape-6-1" points="65.06 51.49 85.74 39.23 49.94 39.23 65.06 51.49" class="cls-1"></polygon>
      <polygon id="shape-4-1" points="0.26 37.78 38.7 37.78 21.04 25.31 0.26 37.78" class="cls-1"></polygon>
      <polygon id="shape-4-2" points="24.65 0 58.26 5.1 90.39 0.63 24.65 0" class="cls-1"></polygon>
      <polygon id="shape-1-2" points="20.39 24 22.75 2.6 0 36.24 20.39 24" class="cls-1"></polygon>
      <polygon id="shape-5-1" points="64.55 52.94 64.54 52.94 47.93 39.47 29.79 57.16 43.12 69.75 72.03 58.77 64.56 52.95 64.55 52.94" class="cls-1"></polygon>
      <polygon id="shape-4-3" points="114.76 39.23 88.59 39.23 66.29 52.45 73.6 58.15 114.76 39.23" class="cls-1"></polygon>
      <polygon id="shape-5-2" points="83.7 33.02 76.96 37.78 115.4 37.78 94.62 25.31 83.7 33.02" class="cls-1"></polygon>
      <polygon id="shape-2-1" points="115.03 40.71 74.89 59.15 88.31 69.6 115.03 40.71" class="cls-1"></polygon>
      <polygon id="shape-1-3" points="42.35 71.02 33.01 62.2 29.57 72.06 55.33 99.92 42.35 71.02" class="cls-1"></polygon>
      <polygon id="shape-7-1" points="46.1 39.23 0.26 39.23 28.63 56.26 46.1 39.23" class="cls-1"></polygon>
      <polygon id="shape-3-1" points="73.85 60.18 59.65 100.61 87.32 70.67 73.85 60.18" class="cls-1"></polygon>
      <polygon id="shape-3-2" points="92.92 2.69 95.27 24 115.57 36.18 92.92 2.69" class="cls-1"></polygon>
      <polygon id="shape-5-3" points="72.3 60.23 43.93 70.99 57.72 101.72 72.3 60.23" class="cls-1"></polygon>
      <polygon id="shape-3-3" points="31.85 61.11 28.3 57.76 1.43 41.63 28.45 70.86 31.85 61.11" class="cls-1"></polygon>
      <polygon id="shape-2-2" points="57.57 26.38 70.53 35.91 58.37 10.31 45.24 35.75 57.57 26.38" class="cls-1"></polygon>
      </svg>
      <div class="loading-name hidden-md-up animated fadeIn">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Loading_Logo.png">
      </div>
</div>