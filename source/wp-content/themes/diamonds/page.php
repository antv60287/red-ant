<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php
  while (have_posts()) : the_post();
?>
<main>
  <button class="arrow-down--home btn btn--img animated fadeIn"><img src="img/arrow-down.png"></button>
  <div id="scroll" class="landing-scene hidden-sm-down">
    <div data-start="width:101%;margin-left:-0.1%;height:100vh;" data-end="width:110%;margin-left:-5%;height:105vh;" class="landing-scene-layers full-height">
      <div data-start="left:0.1%" data-end="left:2%" class="layer-all-mountains"><img src="img/landing/Landing02.png"></div>
      <div data-start="width:100%" data-end="width:95%;" class="layer-two-mountains"><img src="img/landing/Landing03.png"></div>
      <div data-start="width:70%" data-end="width:55%;" class="layer-one-mountains"><img src="img/landing/Landing04.png"></div>
    </div>
  </div>
  <div class="landing-scene-mobile full-height hidden-md-up"></div>
  <div class="landing-text">
    <div class="div-center">
      <div class="text-xs-center white animated fadeIn">
        <h1 class="white"><?php the_title(); ?></h1>
        <h2 class="h6"><?php the_content(); ?></h2>
      </div>
    </div>
  </div>
  <div class="bg-white"></div>
</main>
<?php endwhile; ?>
<?php get_footer(); ?>