<?php if (!isset($count)) {
    $count = 4;
    $showCountTitle = 0;
} ?>


<section class="story__s0<?php echo $count; ?>">
  <div class="story__s0<?php echo $count; ?>__hero full-height" style="background-image: url(<?php echo get_field('banner_image'); ?>); !important;">
    <div class="container div-center">
      <div class="text-xs-center text--shadow">
        <?php if ($showCountTitle) : ?><p class="text--number white"><?php echo $count; ?></p><?php endif; ?>
        <h2 class="h1 white"><?php the_title(); ?></h2>
      </div>
      <div class="arrow-down">
        <button id="go-to-0<?php echo $count; ?>-text" class="btn--arrow-down btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="arrow-down"></button>
      </div>
    </div>
  </div>
  <div id="0<?php echo $count; ?>-text__anchor" class="bg-white">
    <div class="story__s0<?php echo $count; ?>__02" style="background-image: url(<?php echo get_field('image'); ?>); !important;">
      <div class="container py-3">
        <div class="row">
          <div class="col-xs-12 col-md-10 offset-md-1 text-xs-center animated-desktop fadeInLeftSoon">
            <h3 class="h2 title"><?php echo get_field('sub_title'); ?></h3>
            <p><?php the_content(); ?></p>
            <a href="<?php echo esc_url(get_permalink(get_field('learn_more'))); ?>" class="btn btn--regular mt-1"><?php echo get_field('learn_more_button_text'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>