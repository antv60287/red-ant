<?php
/*
Template Name: Template 2
*/

get_header(); ?>

<?php
while (have_posts()) : the_post();
  ?>
  <body class="story">
  <?php include '_nav.php'; ?>
  <main>
  	<?php include '_content-loading.php'; ?>
    <?php include_once '_content-template2.php'; ?>
  </main>
<?php endwhile; ?>

<?php get_footer(); ?>