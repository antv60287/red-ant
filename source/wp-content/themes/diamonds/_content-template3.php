<?php if (!isset($count)) {
    $count = 3;
    $showCountTitle = 0;
} ?>


<section class="story__s0<?php echo $count; ?>">
  <div class="story__s0<?php echo $count; ?>__hero full-height" style="background-image: url(<?php echo get_field('banner_image'); ?>); !important;">
    <div class="container div-center">
      <div class="text-xs-center text--shadow">
        <?php if ($showCountTitle) : ?><p class="text--number white"><?php echo $count; ?></p><?php endif; ?>
        <h2 class="h1 white"><?php the_title(); ?></h2>
      </div>
      <div class="arrow-down">
        <button id="go-to-0<?php echo $count; ?>-text" class="btn--arrow-down btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="arrow-down"></button>
      </div>
    </div>
  </div>
  <div id="0<?php echo $count; ?>-text__anchor" class="bg-white" >
    <div class="container py-3">
      <div class="row">
        <div class="col-xs-12 col-sm-6 push-sm-6 text-xs-center">
          <div class="story__s0<?php echo $count; ?>__text">
            <div class="animated-desktop fadeInRightSoon">
              <h3 class="h2 title"><?php echo get_field('sub_title'); ?></h3>
              <p><?php the_content(); ?></p>
            </div><img src="<?php echo get_field('image_2'); ?>" alt="<?php the_title(); ?>" class="hidden-md-down animated-desktop fadeInRightSoon">
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 pull-sm-6">
            <img src="<?php echo get_field('image_1'); ?>" alt="<?php the_title(); ?>" class="animated-desktop fadeInLeftSoon">
        </div>
      </div>
    </div>
  </div>
</section>