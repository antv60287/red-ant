<?php
/*
Template Name: Story
*/

get_header(); ?>

<?php
while (have_posts()) : the_post();
    ?>
    <body class="story">
    <?php include '_nav.php'; ?>
    <main>
        <?php include '_content-loading.php'; ?>
        <?php
        $count = 1;
        $showCountTitle = 1;
        $page_title = get_the_title();
        while (have_rows('select_page')): the_row();
            $post_object = get_sub_field('page_selected');
            if ($post_object):
                // override $post
                $post = $post_object;
                setup_postdata($post);
                $template = get_page_template_slug(get_the_ID());
                switch ($template) {
                    case 'page-template1.php' :
                        include '_content-template1.php';
                        break;
                    case 'page-template2.php' :
                        include '_content-template2.php';
                        break;
                    case 'page-template3.php' :
                        include '_content-template3.php';
                        break;
                    case 'page-template4.php' :
                        include '_content-template4.php';
                        break;
                    case 'page-template5.php' :
                        include '_content-template5.php';
                        break;
                    default: continue;
                }
                $count++;
                wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
        endif;
        endwhile;
        ?>

        <div class="nav--right">
            <ul class="nav--right__list">
            <?php
                $count = 1;
                while (have_rows('select_page')): the_row();
                    $post_object = get_sub_field('page_selected');
                    if ($post_object):
                        // override $post
                        $post = $post_object;
                        setup_postdata($post);
            ?>
                        <li class="nav--right__list__item-0<?php echo $count; ?>">
                            <button id="go-to-0<?php echo $count++; ?>">
                                <span class="hidden-sm-down"><?php the_title(); ?></span>
                            </button>
                        </li>
                    <?php
                        wp_reset_postdata();
                        endif;
                     ?>
            <?php endwhile; ?>
            </ul>
        </div>
    </main>
<?php endwhile; ?>

<?php get_footer(); ?>