<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Australian Diamonds
 * @since Australian Diamonds
 */

get_header(); ?>

<?php
  // Start the loop.
  while ( have_posts() ) : the_post();


  $imageObj = get_field('post-image');
  $imgUrl = $imageObj['url'];

  global $post;
?>


<main class="main">
  <div class="head-nav">
    <a href="#" class="head-nav__item"><i class="fa fa-long-arrow-left"></i><span>BACK TO PREVIOUS PAGE</span></a>
    <?php if (check_post_pin($post->ID)) { ?>
      <span data-pin class="head-nav__item pin-action __pin" pin-type="post" pin-id="<?php echo $post->ID; ?>" ><i class="__pin-icon glyphicon glyphicon-pushpin"></i><span class="pin-title">PIN THIS PAGE</span></span>
    <?php } else { ?>
      <span data-pin class="head-nav__item remove-pin-action __pin" pin-type="post" pin-id="<?php echo $post->ID; ?>" ><i class="__pin-icon fa fa-times-circle"></i><span class="pin-title">UNPIN THIS PAGE</span></span>
    <?php } ?>
  </div>
  <h2 class="sub-title"><?php the_title(); ?></h2>
  <ul role="tablist" class="nav nav-tabs">
    <li role="presentation"><a href="<?php echo get_post_permalink($post->ID) ?>" class="active">CURRENT STORY: <?php the_title(); ?></a></li>
    <li role="presentation">
      <?php
          $categories = get_the_category($post->ID);
          foreach ($categories as $cat) {
            // var_dump($cat);
            $categories_link = get_category_link($cat->cat_ID);
            echo "<a href='$categories_link'>News List</a>";
          }
       ?>
    </li>
  </ul>
  <div class="news-post">
    <div class="news-post__img"><img src="<?php echo $imgUrl; ?>" alt="" class="img-responsive"></div>
    <div class="news-post__body">
      <h3 class="news-post__body__title"><?php the_title(); ?></h3>
      <div class="news-post__body__content">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
</main>

<input type="hidden" id="nextpage_url" value="<?php echo get_field('next_page'); ?>" >

<?php endwhile; ?>

<?php get_footer(); ?>
