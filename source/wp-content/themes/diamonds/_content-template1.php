<?php if (!isset($count)) {
    $count = 1;
    $showCountTitle = 0;
} ?>

<section class="story__s0<?php echo $count; ?>">
  <div class="story-bg" style="background-image: url(<?php echo get_field('banner_image'); ?>); !important;"></div>
  <div class="story__s0<?php echo $count; ?>__hero">
    <div class="container div-center div-center--top animated fadeIn">
      <div data-anchor-target=".story__s0<?php echo $count; ?>__desc" data-top-bottom="opacity:0" data-center="opacity:1" class="row text-xs-center story__s0<?php echo $count; ?>__desc">
        <div class="col-md-10 offset-md-1 text--shadow">
          <h1 class="sr-only"><?php $page_title ?></h1>
          <div class="text--lg white"><?php the_content(); ?></div>
        </div>
        <div class="arrow-down">
          <button id="go-to-next" class="btn--arrow-down btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="arrow-down"></button>
        </div>
      </div>
    </div>
    <div id="next__anchor" class="container div-center">
      <div data-anchor-target=".story__s0<?php echo $count; ?>__title" data-bottom-top="opacity:0" data-center="opacity:1" class="text--shadow text-xs-center story__s0<?php echo $count; ?>__title">
        <?php if ($showCountTitle) : ?><p class="text--number white"><?php echo $count; ?></p><?php endif; ?>
        <h2 class="h1 white"><?php the_title(); ?></h2>
      </div>
      <div class="arrow-down">
        <button id="go-to-0<?php echo $count; ?>-text" class="btn--arrow-down btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="arrow-down"></button>
      </div>
    </div>
  </div>
  <div id="0<?php echo $count; ?>-text__anchor" class="bg-white" >
    <div class="container pt-3 story__s0<?php echo $count; ?>__content">
      <div class="row">
        <div class="col-xs-12 col-sm-5 push-sm-7 text-xs-center">
          <div class="story__s0<?php echo $count; ?>__text animated-desktop fadeInRightSoon">
            <h3 class="h2 title"><?php echo get_field('title_1'); ?></h3>
            <p><?php echo get_field('content_1'); ?></p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-7 pull-sm-5 mb-3">
            <img src="<?php echo get_field('image_1_1'); ?>" alt="<?php echo get_field('title_1'); ?>" class="story__s0<?php echo $count; ?>__img0<?php echo $count; ?> animated-desktop fadeInLeftSoon">
            <img src="<?php echo get_field('image_1_2'); ?>" alt="<?php echo get_field('title_1'); ?>" class="story__s0<?php echo $count; ?>__img02 animated-desktop fadeInRightSoon">
        </div>
      </div>
    </div>
    <div class="story__s0<?php echo $count; ?>__blog text-xs-center">
      <div class="container py-3 story__s0<?php echo $count; ?>__blog--container">
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="card animated-desktop fadeInLeftSoon"><img src="<?php echo get_field('left_image'); ?>" alt="<?php echo get_field('left_titile'); ?>" class="img-100">
              <div class="card-block card-block--blog text-xs-center pt-3">
                <p class="title--blog"><?php echo get_field('left_titile'); ?></p>
                <p class="title--blog--sub"><?php echo get_field('left_sub_titile'); ?></p>
                <div class="card-text">
                  <div><?php echo get_field('left_content'); ?></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="card animated-desktop fadeInLeftSoon"><img src="<?php echo get_field('right_image'); ?>" alt="<?php echo get_field('right_titile'); ?>" class="img-100">
              <div class="card-block card-block--blog text-xs-center pt-3">
                <p class="title--blog"><?php echo get_field('right_titile'); ?></p>
                <p class="title--blog--sub"><?php echo get_field('right_sub_titile'); ?></p>
                <div class="card-text">
                  <div><?php echo get_field('right_content'); ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>