<?php
/*
Template Name: Journey
*/

get_header(); ?>

<?php
while (have_posts()) : the_post();
?>
<body class="journey">
    <?php include '_nav.php'; ?>
    <main>
        <?php include '_content-loading.php'; ?>
        <!-- <section id="journey-video">
            <div class="journey__s01 full-height">
                <h1 class="sr-only">The Journey</h1>
                <div class="arrow-down">
                    <button id="go-to-next" class="btn--arrow-down btn btn--img"><?php echo get_field('scroll_text_1'); ?><br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down-small.png" alt="Go down"></button>
                </div>
            </div>
        </section> -->

        <section id="journey-video">
            <div class="journey__s01 full-height">
                <h1 class="sr-only">The Journey</h1>
                <div class="video-foreground"></div>
                <div class="arrow-down">
                    <button id="go-to-next" class="btn--arrow-down btn btn--img"><?php echo get_field('scroll_text_1'); ?><br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down-small.png" alt="Go down"></button>
                </div>
        </section>

        <section id="next__anchor">
            <div class="journey-bg"></div>
            <div class="journey__s02 full-height">
                <div class="container div-center">
                    <div class="row">
                        <div class="col-md-10 offset-md-1 text-xs-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/journey/TheJourney_Pin_Small.png" alt="TheJourney_Pin_Small" class="pb-1">
                            <div>
                                <?php the_content(); ?>
                            </div>
                            <!-- <p class="s-26">Natural, authentic and distinctive from the beginning, Australian diamonds carry the heritage of their Australian roots with them forever.</p>
                            <p class="s-26 mb-3">From the moment they are responsibly unearthed, Rio Tinto's unique tracking system carefully monitors their journey as they are cut, polished, and crafted into beautiful jewellery.</p> -->
                        </div>
                    </div>
                </div>
                <div data-anchor-target=".journey__s02" data-bottom-top="opacity:0" data-center="opacity:1" class="arrow-up hidden-sm-down">
                    <button id="go-to-top" class="btn--arrow-up btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-up.png" alt="Go up"><br><?php echo get_field('back_button'); ?></button>
                </div>
                <div class="arrow-down">
                    <button id="go-to-process" class="btn--arrow-down btn btn--img"><?php echo get_field('scroll_text_2'); ?><br><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down-small.png" alt="Go down"></button>
                </div>
            </div>
        </section>

        <section>
            <div class="journey__s03">
                <div id="journey-process__anchor" class="container">
                    <div data-anchor-target=".journey__s03" data-bottom-top="opacity:0" data-center="opacity:1" class="arrow-up">
                        <button id="go-to-description" class="btn--arrow-up btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-up.png" alt="Go up"><br><?php echo get_field('back_button'); ?></button>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-7">
                            <div class="slick--process animated-desktop fadeInLeftSoon">
                                <?php
                                    while (have_rows('journey')): the_row();
                                ?>
                                    <div class="slick--process__single text-xs-center text-md-left">
                                        <img src="<?php echo get_sub_field('icon'); ?>" class="hidden-md-up icon">
                                        <p class="pale text--bold s-16 hidden-md-up"><?php echo get_sub_field('title'); ?></p>
                                        <div><?php echo get_sub_field('content'); ?></div>
                                    </div>
                                    <div class="hidden-md-up text-xs-center tracking-icon">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/journey/TheJourney_DiamondsTracking.png" width="185">
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-5 hidden-sm-down">
                            <div class="slick--process__nav-wrapper animated-desktop fadeInRightSoon">
                                <ul class="slick--process__nav">
                                    <?php
                                        $count = 0;
                                        while (have_rows('journey')): the_row();
                                    ?>
                                        <li class="process">
                                            <button data-slide="<?php echo $count; ?>" class="btn btn--img <?php echo $count++ == 0 ? 'active' : ''; ?>"><img src="<?php echo get_sub_field('icon'); ?>"><?php echo get_sub_field('title'); ?></button>
                                        </li>
                                        <?php if ($count < count(get_field('journey'))) { ?>
                                            <li class="marker">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/journey/TheJourney_Pin.png" width="11">
                                                <span data-marker="<?php echo $count - 1; ?>"><?php echo get_field('pin_text'); ?></span>
                                            </li>
                                        <?php } ?>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php endwhile; ?>

<?php get_footer(); ?>