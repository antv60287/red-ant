<?php
/**
 * Australian Diamonds functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Australian Diamonds
 * @since Australian Diamonds 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Australian Diamonds 1.0
 */
session_start();

add_action( 'init', 'cptui_register_my_page_category' );
function cptui_register_my_page_category() {
    $labels = array(
        "name" => __( 'Page Categories', '' ),
        "singular_name" => __( 'Page Category', '' ),
        "menu_name" => __( 'Categories', '' ),
    );

    $args = array(
        "label" => __( 'Page Categories', '' ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => true,
        "label" => "Page Categories",
        "show_ui" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'page_category', 'with_front' => true, ),
        "show_admin_column" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "show_in_quick_edit" => 1,
    );
    register_taxonomy( "template_page_category", array( "page" ), $args );
}

//REGISTER MAIN MENU
function register_main_menu()
{
    register_nav_menu('main-menu', 'Main Menu');
    register_nav_menu('top-menu', 'Top Menu');
    register_nav_menu('language-menu', 'Language Menu');
}

add_action('init', 'register_main_menu');

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

//ADD IMAGE PAGE TEMPLATE FOR PAGE
add_action( 'add_meta_boxes', 'add_attach_thumbs_meta_b' );
function add_attach_thumbs_meta_b (){
    add_meta_box ('att_thumb_display', 'Template Screenshots','render_attach_meta_b','page');
}
function render_attach_meta_b() {
    echo '<p style="float:left; margin-right: 10px;">Template 1<br><a href="' . get_template_directory_uri().'/page-template/template1.jpg'. '" target="_blank"><img src="' . get_template_directory_uri().'/page-template/template1.jpg'. '" width="250" /></a></p>';
    echo '<p style="float:left; margin-right: 10px;">Template 2<br><a href="' . get_template_directory_uri().'/page-template/template2.jpg'. '" target="_blank"><img src="' . get_template_directory_uri().'/page-template/template2.jpg'. '" width="250" /></a></p>';
    echo '<p style="float:left; margin-right: 10px;">Template 3<br><a href="' . get_template_directory_uri().'/page-template/template3.jpg'. '" target="_blank"><img src="' . get_template_directory_uri().'/page-template/template3.jpg'. '" width="250" /></a></p>';
    echo '<p style="float:left; margin-right: 10px;">Template 4<br><a href="' . get_template_directory_uri().'/page-template/template4.jpg'. '" target="_blank"><img src="' . get_template_directory_uri().'/page-template/template4.jpg'. '" width="250" /></a></p>';
    echo '<p style="float:left; margin-right: 10px;">Template 5<br><a href="' . get_template_directory_uri().'/page-template/template5.jpg'. '" target="_blank"><img src="' . get_template_directory_uri().'/page-template/template5.jpg'. '" width="250" /></a></p>';
    echo '<div style="clear:both;"></div>';
}

//OTHER FUNCTIONS
add_filter( 'show_admin_bar', '__return_false' );//REMOVE ADMIN BAR
// add_filter('acf/settings/show_admin', '__return_false');//HIDE ACF MENU


function show_language_menu() {
    $menuParameters = array(
        'theme_location' => 'language-menu',
        'container'       => false,
        'echo'            => false,
        'items_wrap'      => '%3$s',
        'depth'           => 0,
    );
    echo strip_tags( wp_nav_menu( $menuParameters ), '<a>' ) ;
}

function handle_save_post( $post_id ) {
    if (get_post_type($post_id) === 'page') {
        $template = get_page_template_slug($post_id);
        $templateList = array (
            'page-template1.php',
            'page-template2.php',
            'page-template3.php',
            'page-template4.php',
            'page-template5.php',
        );
        if (in_array($template,$templateList)) {
            $storyTerm = get_term_by('slug', 'story-template', 'template_page_category');
            wp_set_post_terms($post_id,$storyTerm->term_id, 'template_page_category');
        }
    }
}
add_action( 'save_post', 'handle_save_post' );