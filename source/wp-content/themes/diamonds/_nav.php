<nav>
    <div class="nav nav--top hidden-sm-down">
        <?php
          if ( has_nav_menu( 'main-menu' )) {
            wp_nav_menu( array( 'theme_location' => 'top-menu', 'menu_class' =>'nav--top__list' ) );
          }
        ?>
        <div class="nav--top__lang-switcher"><?php show_language_menu(); ?></div>
    </div>
    <button id="nav__button" class="btn btn--img nav__button">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/nav/Nav.png" alt="Nav" class="nav__button__image">
        <div class="sr-only">Menu</div>
    </button>
    <div class="nav--full animated fadeOutUp">
        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/nav/Logo_Orange.png" alt="logo"></a>
        <?php
          if ( has_nav_menu( 'main-menu' )) {
            wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' =>'nav--full__list' ) );
          }
        ?>
        <div class="nav--full__lang-switcher animated"><?php show_language_menu(); ?></div>
    </div>
</nav>
