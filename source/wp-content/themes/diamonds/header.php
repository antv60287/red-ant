<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Australian Diamonds
 */
$template_dir = get_template_directory_uri();
?>

<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Australian Diamonds">
  <meta name="keywords" content="Australian Diamonds">

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $template_dir; ?>/assets/img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" href="<?php echo $template_dir; ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo $template_dir; ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo $template_dir; ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="<?php echo $template_dir; ?>/assets/img/favicon/manifest.json">
  <link rel="shortcut icon" href="<?php echo $template_dir; ?>/assets/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#faa41a">
  <meta name="msapplication-TileImage" content="<?php echo $template_dir; ?>/assets/<?php echo $template_dir; ?>/assets/img/favicon/ms-icon-144x144.png">
  <meta name="msapplication-config" content="<?php echo $template_dir; ?>/assets/<?php echo $template_dir; ?>/assets/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <meta name="application-name" content="Australian Diamonds">
  <link rel="stylesheet" type="text/css" href="<?php echo $template_dir; ?>/assets/css/main.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $template_dir; ?>/style.css">
  <title><?php wp_title('|' , true, 'right' ); ?><?php bloginfo('name'); ?></title>
  <?php wp_head(); ?>
  <?php
    $currentTemplate = get_page_template_slug(get_the_ID());
    if ($currentTemplate === 'page-template3.php') :
      include('_style_template3.php');
    else :
      while (have_rows('select_page')): the_row();
          $post_object = get_sub_field('page_selected');
          if ($post_object):
              // override $post
              $post = $post_object;
              setup_postdata($post);
              $template = get_page_template_slug(get_the_ID());
              if ($template === 'page-template3.php') :
                include('_style_template3.php');
              endif;
          endif;
              wp_reset_postdata();
      endwhile;
    endif;
    ?>

</head>