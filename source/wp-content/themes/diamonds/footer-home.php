<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Australian Diamonds
 * @since Australian Diamonds
 */
$template_dir = get_template_directory_uri();
?>
  <?php include_once '_scripts.php' ?>
  <?php wp_footer(); ?>
</body>