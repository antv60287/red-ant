<script src="<?php echo $template_dir; ?>/assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $template_dir; ?>/assets/js/vendor/skrollr.min.js"></script>
<script src="<?php echo $template_dir; ?>/assets/js/vendor/jquery.nicescroll.js"></script>
<script src="<?php echo $template_dir; ?>/assets/js/vendor/slick.min.js"></script>
<script src="<?php echo $template_dir; ?>/assets/js/vendor/preloadjs-0.6.2.combined.js"></script>
<script src="<?php echo $template_dir; ?>/assets/js/vendor/snap.svg-min.js"></script>
<script src="<?php echo $template_dir; ?>/assets/js/main.min.js"></script>
<script src="<?php echo $template_dir; ?>/main-custom.js"></script>
