<?php if (!isset($count)) {
    $count = 5;
    $showCountTitle = 0;
} ?>

<section class="story__s05">
  <div class="story__s05__hero full-height" style="background-image: url(<?php echo get_field('banner_image'); ?>); !important;">
    <div class="container div-center">
      <div class="text-xs-center text--shadow">
        <?php if ($showCountTitle) : ?><p class="text--number white"><?php echo $count; ?></p><?php endif; ?>
        <h2 class="h1 white"><?php the_title(); ?></h2>
      </div>
      <div class="arrow-down">
        <button id="go-to-05-text" class="btn--arrow-down btn btn--img"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="arrow-down"></button>
      </div>
    </div>
  </div>
  <div id="05-text__anchor" class="bg-white">
    <div class="container py-3">
      <div class="row">
        <div class="col-xs-12 col-sm-5 text-xs-center">
          <div class="story__s05__text animated-desktop fadeInLeftSoon">
            <h3 class="h2 title"><?php echo get_field('sub_title'); ?></h3>
            <p><?php the_content(); ?></p>
            <a href="<?php echo esc_url(get_permalink(get_field('learn_more'))); ?>" class="btn btn--regular mb-2"><?php echo get_field('learn_more_button_text'); ?></a>
          </div>
        </div>
        <div class="col-xs-12 col-sm-7 mb-2">
            <img src="<?php echo get_field('image_1'); ?>" alt="<?php the_title(); ?>" class="story__s05__img01 animated-desktop fadeInRightSoon">
            <img src="<?php echo get_field('image_2'); ?>" alt="<?php the_title(); ?>" class="story__s05__img02 animated-desktop fadeInRightSoon">
            <img src="<?php echo get_field('image_3'); ?>" alt="A symbol of authenticity" class="story__s05__img03 animated-desktop fadeInRightSoon"></div>
      </div>
    </div>
  </div>
</section>