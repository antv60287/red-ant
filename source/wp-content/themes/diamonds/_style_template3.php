<style type="text/css">
  .story__s03__hero {
      background-image: url(<?php echo get_field('banner_image_mobile'); ?>);
  }
  @media (min-width: 768px) {
    .story__s03__hero {
      background-image: url(<?php echo get_field('banner_image'); ?>);
    }
  }
</style>