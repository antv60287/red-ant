<?php
/*
Template Name: Promise
*/

get_header(); ?>

<?php
while (have_posts()) : the_post();
?>
<body class="promise">
    <?php include '_nav.php'; ?>
    <main>
        <?php include '_content-loading.php'; ?>
        <section>
            <div data-start="background-position: 50% 0px;" data-end="background-position: 50% -80px;" class="promise-bg" style="background-image: url(<?php echo get_field('banner_image'); ?>); !important;"></div>
            <div class="promise-bg-overlay hidden-md-up"></div>
            <div class="container">
                <div class="row animated fadeIn">
                    <div class="col-xs-12 col-md-6 offset-md-6 text-xs-center white promise__s01__text01 animated-desktop fadeInRightSoon">
                        <h1 class="white title"><?php the_title(); ?></h1>
                        <div><?php the_content(); ?></div>
                        <!-- <p class="s-18">When you purchase an Australian diamond you can wear it with pride, because not only do you know it’s been ethically sourced and responsibly produced by one of the world’s most trusted mines, you also know you’re getting a genuine diamond with a story and heritage like no other. Rio Tinto, a global industry leader with a reputation for trust and integrity towards its consumers, workers, communities and the environment, carefully traces the journey of your Australian diamonds through a unique tracking system.</p>
                        <p class="s-18">Certified by independent auditors, this system stringently monitors every movement of these beautiful gems, from the moment they are unearthed responsibly at the Argyle Diamond Mine, until they are cut and polished and ultimately crafted into jewellery by expert diamantaires. That’s why each time you purchase a piece of jewellery made with these gems you will receive a Certificate of Authenticity from Rio Tinto so you can be sure your diamonds are natural, authentic and uniquely Australian.</p>
                        <img src="img/promise/ThePromise_Image01.png" alt="The Promise"> -->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 offset-md-6 text-xs-center white py-3">
                        <div class="promise__s01__text02 animated-desktop fadeInRightSoon my-2">
                            <h2 class="pale title"><?php echo get_field('second_title'); ?></h2>
                            <!-- <h2 class="pale title">Always ask for australian diamonds</h2> -->
                            <div><?php echo get_field('second_content'); ?></div>
                            <!-- <p class="s-18">Australian diamond jewellery is available internationally at selected jewellers. These brands are authorized by Rio Tinto to use the Australian Diamonds™ assurance mark, which means they are part of the stringent Chain of Custody that guarantees you are getting a genuine Australian diamond from the Argyle Diamond Mine.</p>
                            <p class="s-18">Beautiful and versatile, Australian diamonds can be used in any design, from high-fashion and bespoke baubles, to everyday accessories. Be sure to ask your authorized jeweller to view their collections featuring Australian Diamonds.</p>
                            <img src="img/promise/ThePromise_Image02.png" alt="Always ask for australian diamonds"> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bg-white"></div>
    </main>
<?php endwhile; ?>

<?php get_footer(); ?>