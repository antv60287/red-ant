<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Australian Diamonds
 * @since Australian Diamonds
 */
$template_dir = get_template_directory_uri();
?>
	<footer>
	    <div class="footer__copyright hidden-sm-down">
	      	<p>Copyrights 2017 &copy; <a href="http://www.riotinto.com/copperanddiamonds/rio-tinto-diamonds-10526.aspx" target="_blank">Rio Tinto</a>. All rights reserved.</p>
	    </div>
  	</footer>
	<input type="hidden" id="base_url" value="<?php echo $template_dir; ?>" />
	<?php include_once '_scripts.php' ?>
	<?php wp_footer(); ?>
</body>