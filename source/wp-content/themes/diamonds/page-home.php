<?php
/*
Template Name: Homepage
*/

get_header(); ?>
<body class="index">
<?php
  while (have_posts()) : the_post();
?>
<main>
	<?php include '_content-loading.php'; ?>

	<div class="landing-scene hidden-sm-down">
		<div data-anchor-target=".landing-scene" data-start="width:calc(100% + 1px);left:-1px;height:calc(100% + 1px);top:-1px" data-end="width:calc(100% + 80px);left:-40px;height:calc(100% + 80px);top:-40px" class="landing-scene-layers full-height"></div>
		<div data-anchor-target=".landing-scene" data-start="width:calc(100% + 1px);left:-1px;bottom:-10px" data-end="width:calc(100% + 50px);left:-25px;bottom:-30px" class="layer-all-mountains">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/Landing02.png">
		</div>
		<div data-anchor-target=".landing-scene" data-start="width:calc(100% + 1px);left:-1px;bottom:-1px" data-end="width:calc(100% + 400px);left:-200px;bottom:-150px" class="layer-two-mountains">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/Landing03.png">
		</div>
		<div data-anchor-target=".landing-scene" data-start="width:50%;left:50%;bottom:-1%" data-end="width:90%;left:40%;bottom:-39%" class="layer-one-mountains">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/landing/Landing04.png">
		</div>
	</div>
	<div class="landing-scene-mobile full-height hidden-md-up"></div>
	<div class="landing-text animated fadeIn">
		<div class="div-center">
			<div class="text-xs-center white">
				<h1 class="white text--bold"><?php the_title(); ?></h1>
				<h2 class="white s-18"><?php the_content(); ?></h2>
			</div>
		</div>
	</div>
	<button class="arrow-down--home btn btn--img animated fadeIn"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow-down.png" alt="Go down"></button>
</main>
<?php endwhile; ?>

<?php get_footer('home'); ?>