<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'redant');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Zwe=:jk4Su3N@mWN$y~IKWFH)K*_r-Ec/:}m!rt1tFKUc[UV.Cr24vS/;FT`bhuP');
define('SECURE_AUTH_KEY',  '&Yo+{5!@V<V5PDraSv002BhPD_1.]xRND>tl5MERCbHrP.]K X bivN/o`)!@18|');
define('LOGGED_IN_KEY',    'Jo,7_XFD7!NJ7%OoBav?e;&,~=pV^s2`Txz?fbR+t-q~TF 2pqym|w)uWtT/vh`L');
define('NONCE_KEY',        '$mGZNjuc2DYie.wwL_i)kXK!,}9xZ3}%bjD2qC2dKXuJ};1<hSV&u8?Su)2^)7y`');
define('AUTH_SALT',        'is#/}!{8tz>4Vbij,%UVtwwa.Bu]mt5FRc0^Bi~XB<yV}I fJS;a3B2DtVk/_=]G');
define('SECURE_AUTH_SALT', 'Ah_0l2IIr }:St(84;uenL|6mw(@oZ1m7]4MY$s<K?R$FyP-Ch~F]mJt[6OzDx)!');
define('LOGGED_IN_SALT',   '|;C2]H+<3Z#tOg{MT (8JgU5f=z*=%0U0=CLMOmd*M1[z2SA&S3V_^uz{,{w3 jf');
define('NONCE_SALT',       'YRqK(M_Ws ,D.J=<t5tki0ZoejR$&M~0|8,?a!2[U]Yl>xAd>N!WBPB.zIA>^#K0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'dms_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
